#!/usr/bin/python
# --coding utf8--

from __future__ import print_function

import socket
import re
from time import sleep
import argparse
from subprocess import check_output,CalledProcessError
import sys


def one_running_instance():
    try:
        instance = check_output(['pgrep', '-f', '"^python.*Marantz.py"'])
    except CalledProcessError as e:
        if e.returncode == 1:
            instance = ''
        else:
            raise(Exception('Unhandled error'))
    nb_instance = len(instance.split('\n'))
    if nb_instance > 2 :
        return False
    return True

def netcat(hostname, port, content):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((hostname, port))
    except socket.error:
        sys.exit(0)

    s.sendall(content)
    sleep(0.1)
    s.shutdown(socket.SHUT_WR)
    while 1:
        data = s.recv(1024)
        if data == "":
            break
        s.close()
        sleep(0.1)
        return repr(data)
    sleep(0.1)
    s.close()


# TODO : rendre g?n?rique cette classe
class Amp():
    _CmdVolume = { 'Status': 'MV?', 'Up': 'MVUP', 'Down': 'MVDOWN', 'Set': 'MV__', }
    _CmdPower = { 'Status': 'PW?', 'On': 'PWON', 'Off': 'PWSTANDBY', }
    _CmdInput = { 'Status': 'SI?', 'MediaPlayer': 'SIMPLAY', 'CD': 'SICD', 'BD/DVD': 'SIBD', 'Game': 'SIGAME',  'Satellite': 'SISAT', 'AUX1': 'SIAUX1', 'AUX2': 'SIAUX2', 'TV': 'SITV', 'Tuner': 'SITUNER', 'InternetRadio': 'SIIRADIO', 'Pandora': 'SIPANDORA', 'Spotify': 'SISPOTIFY', 'Server': 'SISERVER', 'Favoris': 'SIFAVORITES'}
    _CmdSurroundMode = { 'Status': 'MS?', 'Movie': 'MSMOVIE', 'Music': 'MSMUSIC', 'Game': 'MSGAME', 'Direct': 'MSDIRECT', 'Pure Direct': 'MSPURE DIRECT', 'Stereo': 'MSSTEREO', 'Auto': 'MSAUTO', 'Neural': 'MSNEURAL', 'Standard': 'MSSTANDARD', 'Dolby': 'MSDOLBY DIGITAL', 'DTS': 'MSDTS SURROUND', 'Multi Channel': 'MSMULTI CH IN', 'Mch Stereo': 'MSMCH STEREO', 'Virtual': 'MSVIRTUAL',}
    _CmdSleep = { 'Status': 'SLP?', 'Set': 'SLP___', 'Off': 'SLPOFF',}

    def __init__(self, hostname=None):
        self.hostname = hostname

    #def __get_hostname(self):
    #    return self.hostname
    #
    #def __set_hostname(self, hostname):
    #    if re.search(r' \b(?:\d{1,3}\.){3}\d{1,3}\b', hostname) or re.search(r'^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$', hostname):
    #        self.hostname = hostname
    #    raise(Exception('Wrong hostname provided. Please enter a hostname or IP hostname compliant value'))
    #
    #hostname = property(__get_hostname, __set_hostname)

    # get a specific line in output. (0=all lines)
    @staticmethod
    def get_line(ret, line=0):
        sep = r'\r'
        result = ret.strip('\'').split(sep)
        if line != 0:
            return result[line-1]
        return result

    def execute_command(self, command):
        return netcat(self.hostname, 23, command+'\r')

    def load_commands(self):
        '''
        Process command and determine which method has to be executed
        '''
        my_attributes = vars(self)
        commands = [ attr for attr in my_attributes if getattr(self, attr) is not None and getattr(self, attr) is not False and attr != 'hostname']
        return commands

    def process_commands(self):
        CmdMapping = { 'volume_status': self.get_volume,
            'volume_up': self.set_volume_up,
            'volume_down': self.set_volume_down,
            'volume_set': self.set_volume,
            'power_status': self.get_power,
            'power_on': self.set_power_on,
            'power_standby': self.set_power_standby,
            'select_input': self.set_input,
            'input_selected': self.get_input,
            'select_surroundmode': self.set_surroundmode,
            'surroundmode_status': self.get_surroundmode,
            'sleep_status': self.get_sleep,
            'sleep_off': self.set_sleep,
            'sleep': self.set_sleep,
        }

        cmd_list = self.load_commands()
        for cmd in cmd_list:
            if isinstance(getattr(self, cmd), bool):
                return CmdMapping[cmd]()
            else:
                return CmdMapping[cmd](getattr(self, cmd))

    def get_human_readable_output(self, hc_response):
        dicts = [Amp._CmdInput, Amp._CmdSurroundMode]
        for d in dicts :
            for k,v in d.iteritems():
                if v.endswith(hc_response):
                    return k

    def set_volume_up(self):
        vol = self.get_volume()
        new_vol = int(vol) + 5
        self.set_volume(new_vol)

    def set_volume_down(self):
        vol = self.get_volume()
        new_vol = int(vol) - 5
        self.set_volume(new_vol)

    def set_volume(self, val):
        cmd = re.sub(r'__$', str(val), Amp._CmdVolume['Set'])
        self.execute_command(cmd)

    def get_volume(self,):
        cmd_ret = self.execute_command(Amp._CmdVolume['Status'])
        vol = Amp.get_line(cmd_ret, line=1)[2:]
        if len(vol) == 3:
            return vol[0:2]
        return vol

    def set_power_on(self):
        self.execute_command(Amp._CmdPower['On'])

    def set_power_standby(self):
        self.execute_command(Amp._CmdPower['Off'])

    def get_power(self):
        cmd_ret = self.execute_command(Amp._CmdPower['Status'])
        pw = Amp.get_line(cmd_ret, line=1)[2:]
        if pw == 'ON':
            return 1
        return 0

    def set_input(self, val):
        self.execute_command(Amp._CmdInput[val])

    def get_input(self):
        cmd_ret = self.execute_command(Amp._CmdInput['Status'])
        hc_ret = Amp.get_line(cmd_ret, line=1)[2:]
        return self.get_human_readable_output(hc_ret)

    def set_surroundmode(self, val):
        self.execute_command(Amp._CmdSurroundMode[val])

    def get_surroundmode(self):
        cmd_ret = self.execute_command(Amp._CmdSurroundMode['Status'])
        hc_ret = Amp.get_line(cmd_ret, line=1)[2:]
        return self.get_human_readable_output(hc_ret)

    def set_sleep(self, val):
        self.execute_command(Amp._CmdSleep[val])

    def get_sleep(self):
        cmd_ret = self.execute_command(Amp._CmdSleep['Status'])
        hc_ret = Amp.get_line(cmd_ret, line=1)[2:]

class MarantzAmp(Amp):

    def __init__(self, ):
        pass

if __name__ == '__main__':
    # TODO : Group options
    parser = argparse.ArgumentParser()

    parser.add_argument('--hostname', '-H', required=True, help='Marantz Ampli Hostname or IP hostname')
    parser.add_argument('--command', '-c', help='Command to execute directly')
    parser.add_argument('--volume-up', '-v+', action='store_true', help='Volume Up')
    parser.add_argument('--volume-down', '-v-', action='store_true', help='Volume Down')
    parser.add_argument('--volume-set', '-v', nargs='?', type=int, choices=range(0,100), help='Set Volume to a value between 0 and 100')
    parser.add_argument('--volume-status', '-vs', action='store_true', help='Volume\'s value')
    parser.add_argument('--power-on', '-po', action='store_true', help='Power on')
    parser.add_argument('--power-standby', '-psb', action='store_true', help='Power Off')
    parser.add_argument('--power-status', '-ps', action='store_true', help='Power\'s value')
    parser.add_argument('--input-selected', '-is', action='store_true', help='Input selected')
    parser.add_argument('--select-input', '-si', nargs="?", choices=[ 'MediaPlayer', 'CD', 'BD/DVD', 'Game',  'Satellite', 'AUX1', 'AUX2', 'TV', 'Tuner', 'InternetRadio', 'Last_Internet_Radio_Played', 'Pandora', ], help='Select an input')
    parser.add_argument('--select-surroundmode', '-ssm', nargs="?", choices=[ 'Status', 'Movie', 'Music', 'Game', 'Direct', 'Pure Direct', 'Stereo', 'Auto', 'Neural', 'Standard', 'Dolby', 'DTS', 'Multi Channel', 'Mch Stereo', 'Virtual',], help='Select a surround mode')
    parser.add_argument('--surroundmode-status', '-sms', action='store_true', help='Surround Mode selected.')
    parser.add_argument('--sleep-status', '-ss', action='store_true', help='Show sleep value')
    parser.add_argument('--sleep-off', '-so', action='store_true', help='Disabled Sleep')
    parser.add_argument('--sleep', '-s', nargs='?', type=int, choices=range(0,121), help='Set sleep value')

    Amp = Amp()
    args = parser.parse_args()
    for k in vars(args).iterkeys():
        setattr(Amp, k, vars(args)[k])

    while 1:
        sleep(0.3)
        if one_running_instance():
            break

    ret = Amp.process_commands()

if ret or ret == 0:
    print(ret)
